The program is structured in 3 java files.

1. The Blueground.java contains the main method that creates the necessary objects and calls the methods.

2. The DailyData.java contains the DailyData class, a class for storing our desired information from the JSON response with setters and getters. 

3. The DataHandler.java contains the DataHandler class which consists of 2 methods.

	a)The method handleJSON(URL myurl) connects to the desired URL, receives the JSON
	response and extracts from it the JSON array "dailysummary".
	Then the required fields are saved in a DailyData object.

	b)The method filewriter(DailyData dd) receives the information we extracted and 
	stored in the DailyData object, and writes the results to a plain text folder
	(in the root folder of the project). 

As a different implementation, instead of holding the data in specific fields in DailyData class, we could implement a HashMap to store 
+the key/value pairs of the JSON response.