
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

/**
 *
 * @author Bazooka
 */
public class DataHandler {
    
     public DailyData handleJSON(URL myurl) throws Exception{        
     
        DailyData dd = new DailyData();
        
        
            JSONTokener tokener = new JSONTokener(myurl.openStream());
            JSONObject root = new JSONObject(tokener);  //root JSON object
            JSONObject history = root.getJSONObject("history");  //the "history" JSON object
            JSONArray daily_arr = history.getJSONArray("dailysummary");  //the daily summary JSON array

            for (int i = 0; i < daily_arr.length(); i++){    //now find the desired fields/values
                dd.setMaxhumidity(daily_arr.getJSONObject(i).getInt("maxhumidity"));
                dd.setMaxtempm(daily_arr.getJSONObject(i).getInt("maxtempm"));
                dd.setMintempm(daily_arr.getJSONObject(i).getInt("mintempm"));
                dd.setPrecipm(daily_arr.getJSONObject(i).getString("precipm"));           
            }
            return dd;   
    }
    
    public void filewriter(DailyData dd) throws Exception{
        BufferedWriter writer = null;
       
            //create file of the data with current timestamp as name
            String time_filename = new SimpleDateFormat( "yyyy_MM_dd HH.mm.ss").format(Calendar.getInstance().getTime());
            File logFile = new File("RESULT " + time_filename);
            System.out.println(logFile.getCanonicalPath()); // file's path shown in the console

            writer = new BufferedWriter(new FileWriter(logFile));
            writer.write(
                    "maxhumidity:"+dd.getMaxhumidity()
                    +"\r\n"+"maxtempm:"+dd.getMaxtempm()
                    +"\r\n"+"mintempm:"+dd.getMintempm()
                    +"\r\n"+"precipm:"+dd.getPrecipm());
       
            writer.close();                 
    }    
}
