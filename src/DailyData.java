/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Bazooka
 */
public class DailyData {

private int maxhumidity;
private int maxtempm;
private int mintempm;
private String precipm;

    public void setMaxhumidity(int maxhumidity) {
        this.maxhumidity = maxhumidity;
    }

    public void setMaxtempm(int maxtempm) {
        this.maxtempm = maxtempm;
    }

    public void setMintempm(int mintempm) {
        this.mintempm = mintempm;
    }

    public void setPrecipm(String precipm) {
        this.precipm = precipm;
    }

    
    
    public int getMaxhumidity() {
        return maxhumidity;
    }

    public int getMaxtempm() {
        return maxtempm;
    }

    public int getMintempm() {
        return mintempm;
    }

    public String getPrecipm() {
        return precipm;
    }

    
    
}
