
import java.net.URL;

/**
 *
 * @author Bazooka
 */
public class Blueground {

    public static void main(String[] args) {
        try{
            
            URL url = new URL ("http://api.wunderground.com/api/bbb1bacd7358969b/history_20160204/q/NY/New_York.json");
            DataHandler dh = new DataHandler();  //create a DataHandler object           
            DailyData dd = dh.handleJSON(url);  // hold in a DailyData object the information we received
            dh.filewriter(dd);     //print information in file
            
        }catch(Exception e){}
        
        
    }
}
